#!/bin/bash

set -e

UPSTREAM_REPO="$BATS_RUN_TMPDIR"/aports_upstream
FORK_REPO="$BATS_RUN_TMPDIR"/aports_fork
BUILD_REPO="$BATS_RUN_TMPDIR"/aports_build

prepare_git() {
    git config --global user.name 'build'
    git config --global user.email 'build@test.local'
    git config --global init.defaultBranch master
}

create_aports_repo() {
    rm -rf "$UPSTREAM_REPO"

    git init -q "$UPSTREAM_REPO"
    pushd "$UPSTREAM_REPO" >/dev/null

    mkdir -p {main,community,testing}
    touch {main,community,testing}/.keep

    git add .
    git commit -qm "initial commit"

    popd >/dev/null
}

clone_fork() {
    git clone -q file://"$UPSTREAM_REPO" "$FORK_REPO"
}

include_package() {
    srcpackage=${1?"Argument #1 'package' is missing"}
    repo=${2?"Argument #2 'repo' is missing"}
    pkgname=${3-$srcpackage}

    cp -r /tests/packages/"$srcpackage" "$repo/$pkgname"
    if [ -f "$repo/$pkgname/APKBUILD.in" ]; then
        PKGNAME=$pkgname envsubst '$PKGNAME' <"$repo/$pkgname"/APKBUILD.in >"$repo/$pkgname"/APKBUILD
    fi
    git add "$repo/$pkgname"
    git commit -qm "$repo/$pkgname: new aport"
}

clone_build_repo() {
    branch=${1?"Provide branch to clone"}
    depth=${2-50}

    git clone -q -b "$branch" --depth "$depth" file://"$FORK_REPO" "$BUILD_REPO"
}

cleanup_repos() {
    if [ -d "$BUILD_REPO" ]; then rm -rf "$BUILD_REPO"; fi
    if [ -d "$FORK_REPO" ]; then rm -rf "$FORK_REPO"; fi
    if [ -d "$UPSTREAM_REPO" ]; then rm -rf "$UPSTREAM_REPO"; fi
}

build_repo() {
    target_branch=${1?"Provide a target branch name"}

    export CI_PROJECT_DIR=$BUILD_REPO
    export CI_MERGE_REQUEST_TARGET_BRANCH_NAME=$target_branch
    export CI_MERGE_REQUEST_PROJECT_URL=file://$UPSTREAM_REPO

    build.sh >"$BATS_RUN_TMPDIR"/build_log.txt 2>&1
}
