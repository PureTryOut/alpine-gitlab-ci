helper_path="/usr/local/share/bats/helpers"

source "$helper_path/bats-support/output.bash"
source "$helper_path/bats-support/error.bash"
source "$helper_path/bats-file/file.bash"
source "$helper_path/bats-assert/assert_failure.bash"

assert_file_contains() {
    pattern=${1?"Missing argument #1: <pattern>"}
    filename=${2?"Missing argument #2: <filename>"}

    if ! grep -q -E "$pattern" "$filename"; then
        batslib_print_kv_single 8 \
            'filename' "$filename" \
            'pattern' "$pattern" \
            | batslib_decorate 'pattern not found' \
            | fail
    fi
}
