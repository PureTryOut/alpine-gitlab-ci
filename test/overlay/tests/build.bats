#!/usr/bin/env bats

load helpers/env
load helpers/common
load helpers/repo

# Just to shut up shellcheck, because bats sets this already
set -e

log() {
    echo "$@" >&3
}

setup() {
    cleanup_repos

    create_aports_repo
    clone_fork

    cd "$FORK_REPO"
}

teardown() {
    if [ "$BATS_ERROR_STATUS" -gt 0 ]; then
        cp "$BATS_RUN_TMPDIR"/build_log.txt "$BATS_TMPDIR/build_$BATS_ROOT_PID.log"
    fi
}

build_base_package() {

    git checkout -qb base-package

    include_package base-package main

    clone_build_repo base-package

    cd "$BUILD_REPO"
    build_repo master
}

@test "build simple change" {
    build_base_package
    assert_exist "$REPODEST/main/$ARCH/base-package-1.0.0-r0.apk"
}

@test 'only builds repositories with changes' {
    cd "$UPSTREAM_REPO"
    include_package base-package main
    include_package pkg-template community community-package

    cd "$FORK_REPO"
    git checkout -qb add-package-testing

    include_package pkg-template testing testing-package

    clone_build_repo add-package-testing

    cd "$BUILD_REPO"
    build_repo master

    assert_exist "$REPODEST/testing/$ARCH/testing-package-1.0.0-r0.apk"
}

@test "test failing package is detected" {
    git checkout -qb failing-package

    include_package failing-package main

    clone_build_repo failing-package

    cd "$BUILD_REPO"
    run build_repo master

    assert_failure
}

@test 'built packages are copied to artifacts' {
    build_base_package
    assert_exist "$BUILD_REPO/packages/main/$ARCH/base-package-1.0.0-r0.apk"
}

@test 'large packages are not copied to artifacts' {
    git checkout -qb large-package

    include_package large-package main

    clone_build_repo large-package

    cd "$BUILD_REPO"
    export MAX_ARTIFACT_SIZE=5000000
    build_repo master

    assert_not_exist "$BUILD_REPO/packages/main/$ARCH/large-package-1.0.0-r0.apk"
}

@test 'buildlogs are written' {
    build_base_package

    assert_exist "$BUILD_REPO/logs/build-base-package.log"
    assert_file_contains "Building main/base-package" "$BUILD_REPO/logs/build-base-package.log"

    assert_exist "$BUILD_REPO/logs/checkapk-base-package.log"
    assert_file_contains "[+]{3} filelist-base-package" "$BUILD_REPO/logs/checkapk-base-package.log"
}

@test 'builds all packages in multiple repos' {
    git checkout -qb add-multiple-packages

    for pkg in a b c d; do
        include_package pkg-template main "package-$pkg"
    done

    for pkg in e f g; do
        include_package pkg-template community "package-$pkg"
    done

    clone_build_repo add-multiple-packages
    cd "$BUILD_REPO"
    build_repo master

    for pkg in a b c d; do
        assert_exist "$REPODEST/main/$ARCH/package-$pkg-1.0.0-r0.apk"
    done

    for pkg in e f g; do
        assert_exist "$REPODEST/community/$ARCH/package-$pkg-1.0.0-r0.apk"
    done
}

@test 'builds no more packages than the limit' {
    git checkout -qb add-multiple-packages

    for pkg in a b c d; do
        include_package pkg-template main "package-limit-$pkg"
    done

    for pkg in e f g; do
        include_package pkg-template community "package-limit-$pkg"
    done

    clone_build_repo add-multiple-packages
    cd "$BUILD_REPO"
    CI_ALPINE_BUILD_LIMIT=6 build_repo master

    for pkg in a b c d; do
        assert_exist "$REPODEST/main/$ARCH/package-limit-$pkg-1.0.0-r0.apk"
    done

    for pkg in e f; do
        assert_exist "$REPODEST/community/$ARCH/package-limit-$pkg-1.0.0-r0.apk"
    done

    assert_not_exist "$REPODEST/community/$ARCH/package-limit-g-1.0.0-r0.apk"
}

@test 'builds correct packages with offset' {
    git checkout -qb add-multiple-packages

    for pkg in a b c d; do
        include_package pkg-template main "package-offset-$pkg"
    done

    for pkg in e f g; do
        include_package pkg-template community "package-offset-$pkg"
    done

    for pkg in h i j; do
        include_package pkg-template testing "package-offset-$pkg"
    done


    clone_build_repo add-multiple-packages
    cd "$BUILD_REPO"
    CI_ALPINE_BUILD_OFFSET=2 CI_ALPINE_BUILD_LIMIT=6 build_repo master

    for pkg in c d; do
        assert_exist "$REPODEST/main/$ARCH/package-offset-$pkg-1.0.0-r0.apk"
    done

    for pkg in e f g; do
        assert_exist "$REPODEST/community/$ARCH/package-offset-$pkg-1.0.0-r0.apk"
    done

    assert_exist "$REPODEST/testing/$ARCH/package-offset-h-1.0.0-r0.apk"

    for pkg in a b; do
        assert_not_exist "$REPODEST/main/$ARCH/package-offset-$pkg-1.0.0-r0.apk"
    done

    for pkg in i j; do
        assert_not_exist "$REPODEST/testing/$ARCH/package-offset-$pkg-1.0.0-r0.apk"
    done
}

@test 'handles deleted packages' {
    cd "$UPSTREAM_REPO"

    include_package base-package main
    include_package pkg-template main a
    include_package pkg-template main b

    cd "$FORK_REPO"
    git pull --quiet --ff-only

    git checkout -qb remove-package
    git mv main/b community/
    git commit -qm 'main/b: remove'

    include_package pkg-template community c

    clone_build_repo remove-package

    cd "$BUILD_REPO"
    export CI_DEBUG_BUILD=1
    build_repo master

    assert_exist "$REPODEST/community/$ARCH/c-1.0.0-r0.apk"
}

@test 'handles moved packages' {
    cd "$UPSTREAM_REPO"

    include_package base-package main
    include_package pkg-template main a
    include_package pkg-template main b

    cd "$FORK_REPO"
    git pull --quiet --ff-only

    git checkout -qb remove-package
    git mv main/b community/
    git commit -qm 'community/b: move from main'

    include_package pkg-template community c

    clone_build_repo remove-package

    cd "$BUILD_REPO"
    export CI_DEBUG_BUILD=1
    build_repo master

    assert_exist "$REPODEST/community/$ARCH/c-1.0.0-r0.apk"
}
