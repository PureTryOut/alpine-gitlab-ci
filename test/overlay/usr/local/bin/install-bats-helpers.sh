#!/bin/sh

set -eu

install_dir="/usr/local/share/bats/helpers"

install_helper() {
    helper=${1?"Argument #1 'helper' missing"}
    version=${2?"Argument #2 'version' missing"}

    mkdir -p /tmp/bats-helpers
    cd /tmp/bats-helpers

    curl -LSs \
        https://github.com/bats-core/"$helper"/archive/"$version".tar.gz \
        | tar xz

    cd "$helper-${version#v}"
    install -dm0755 "$install_dir"
    cp -r src "$install_dir/$helper"

    rm -r /tmp/bats-helpers
}

install_helper bats-support v0.3.0
install_helper bats-assert  0a8dd57e2cc6d4cc064b1ed6b4e79b9f7fee096f
install_helper bats-file    v0.3.0
