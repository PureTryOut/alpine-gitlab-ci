ARG DOCKER_ARCH=
ARG ALPINE_ARCH=
FROM alpinelinux/build-base:latest$ALPINE_ARCH

COPY overlay/ /

RUN git config --global init.defaultBranch master

ENV SUDO=doas
